package interfaces;

import java.util.List;

public class CSVExport implements Export {
    public String export(List<String> data) {
        String result = "{\n";
        for (String d : data) {
            result += "[" + d + "]\n";
        }
        result += "}";
        return result;
    }
}
